#include <chrono>
#include <cstdint>
#include <sdbus-c++/sdbus-c++.h>
#include <rapidxml/rapidxml.hpp>
#include <sstream>
#include <thread>
#include <unistd.h>
#include <vector>
#include <string>
#include <iostream>
#include <common.hpp>

void setup(sdbus::IConnection& con, const char* service, const char* agentPath, std::string stationPath)
{
	auto stationProxy = sdbus::createProxy(con, "net.connman.iwd", stationPath);
	auto method = stationProxy->createMethodCall("net.connman.iwd.Station", "RegisterSignalLevelAgent");
	std::vector<int16_t> levels = {-40, -50, -60};
	method << sdbus::ObjectPath(agentPath) << levels;
	stationProxy->callMethod(method);
}


int main(int argc, char *argv[])
{
	std::unique_ptr<sdbus::IConnection> connection;
	const char* serviceName = "org.sdbuscpp.iwdinfo";
	const char* objectPath = "/org/sdbuscpp/iwdinfo";
	std::unique_ptr<sdbus::IObject> iwdinfo;
	std::string stationPath;
	std::shared_ptr<sdbus::IProxy> stationProxy;
	try {
		connection = sdbus::createSystemBusConnection(serviceName);
	} catch (...) {
		std::cerr << "Failed to open System Bus Connection" << std::endl;
		return -1;
	}
	try {
		iwdinfo = sdbus::createObject(*connection, objectPath);
	} catch (...) {
		std::cerr << "Failed to create iwdinfo object" << std::endl;
		return -1;
	}
	try {
		stationPath = getStationPath(*connection, "/net/connman/iwd/0");
	} catch (...) {
		std::cerr << "Failed to get station path" << std::endl;
		return -1;
	}
	try {
		stationProxy = sdbus::createProxy(*connection, "net.connman.iwd", stationPath);
	} catch (...) {
		std::cerr << "Failed to create station path" << std::endl;
		return -1;
	}
	auto changed = [&con=*connection, &stationProxy, &iwdinfo](const sdbus::ObjectPath object, const uint8_t level)
	{
		sdbus::ObjectPath networkPath = stationProxy->getProperty("ConnectedNetwork").onInterface("net.connman.iwd.Station");
		auto netProxy = sdbus::createProxy(con, "net.connman.iwd", networkPath);
		std::string name = netProxy->getProperty("Name").onInterface("net.connman.iwd.Network");
		std::ostringstream stream;
		stream << "{";
		stream << "\"objectPath\":\"" << object << "\",";
		stream << "\"name\":\"" << name << "\",";
		stream << "\"level\":\"" << (int) level << "\",";
		std::string icon = "󰤯";
		switch ((int) level){
			case 0:
				icon = "󰤨";
				break;
			case 1:
				icon = "󰤥";
				break;
			case 2:
				icon = "󰤢";
				break;
			case 3:
				icon = "󰤟";
				break;
		}
		stream << "\"icon\":\"" << icon << "\"";
		stream << "}";
		const char* interfaceName = "net.connman.iwd.SignalLevelAgent";
		std::cout << stream.str() << std::endl;
		iwdinfo->emitSignal("onChange").onInterface(interfaceName).withArguments(stream.str());
		return;
	};
	auto release = [](const sdbus::ObjectPath object)
	{
		return;
	};
	try {
		const char* interfaceName = "net.connman.iwd.SignalLevelAgent";
		iwdinfo->registerMethod("Changed").onInterface(interfaceName).implementedAs(std::move(changed));
		iwdinfo->registerMethod("Release").onInterface(interfaceName).implementedAs(std::move(release));
		iwdinfo->registerSignal("onChange").onInterface(interfaceName).withParameters<std::string>();
		iwdinfo->finishRegistration();
	} catch (...) {
		std::cerr << "Failed to register iwdinfo" << std::endl;
		return -1;
	}
	connection->enterEventLoopAsync();
	try {
		setup(*connection, serviceName, objectPath, stationPath);
	} catch (...) {
		std::cerr << "Failed to setup agent" << std::endl;
		return -1;
	}

	// Run the loop on the connection.
	connection->enterEventLoop();
	return 0;
}
