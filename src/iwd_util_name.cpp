#include <map>
#include <sdbus-c++/Error.h>
#include <common.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>


int main(int argc, char *argv[])
{
	auto conn = sdbus::createConnection();
	const char* destinationName = "net.connman.iwd";
	auto objectPath = getStationPath(*conn, "/net/connman/iwd/0");
	auto concatenatorProxy = sdbus::createProxy(*conn, destinationName, objectPath);

	const char* interfaceName = "net.connman.iwd.Station";
	try {
		sdbus::ObjectPath networkPath = concatenatorProxy->getProperty("ConnectedNetwork").onInterface(interfaceName);
		auto netProxy = sdbus::createProxy(*conn, destinationName, networkPath);
		std::string name = netProxy->getProperty("Name").onInterface("net.connman.iwd.Network");
		std::cout << name << std::endl;

	}
	catch (sdbus::Error& x){}

	return 0;
}
