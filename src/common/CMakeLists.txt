add_library(Common common.cpp)
target_link_libraries(Common PUBLIC SDBusCpp::sdbus-c++)
target_include_directories(Common PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
