#include <common.hpp>
#include <rapidxml/rapidxml.hpp>

std::string getStationPath(sdbus::IConnection& con, std::string devicePath)
{
	auto proxy = sdbus::createProxy(con, "net.connman.iwd", devicePath);
	auto method = proxy->createMethodCall("org.freedesktop.DBus.Introspectable", "Introspect");
	auto result = proxy->callMethod(method);
	std::string x;
	result >> x;
	rapidxml::xml_document<> doc;
	doc.parse<0>(x.data());
	std::string stationPath = devicePath + "/" + doc.first_node()->first_node("node")->first_attribute("name")->value();
	return stationPath;
}
