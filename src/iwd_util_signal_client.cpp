#include <iostream>
#include <sdbus-c++/sdbus-c++.h>

int main(int argc, char* argv[])
{
	auto conn = sdbus::createConnection();
	auto destinationName = "org.sdbuscpp.iwdinfo";
	auto objectPath = "/org/sdbuscpp/iwdinfo";
	auto iwdinfoProxy = sdbus::createProxy(*conn, destinationName, objectPath);

	auto interfaceName = "net.connman.iwd.SignalLevelAgent";
	iwdinfoProxy->uponSignal("onChange").onInterface(interfaceName).call([](const std::string& str){
		std::cout << str << std::endl;
	});
	iwdinfoProxy->finishRegistration();
	conn->enterEventLoop();
	return 0;
}
