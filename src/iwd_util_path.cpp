#include <iostream>
#include <common.hpp>

int main()
{
	std::string path = "/net/connman/iwd/0";
	auto proxy = sdbus::createConnection();
	path = getStationPath(*proxy, path);
	std::cout << path << std::endl;
	return 0;
}
