#include <chrono>
#include <cstdint>
#include <sdbus-c++/sdbus-c++.h>
#include <rapidxml/rapidxml.hpp>
#include <thread>
#include <unistd.h>
#include <vector>
#include <string>
#include <iostream>
#include <common.hpp>

void setup(sdbus::IConnection& con, const char* service, const char* agentPath, std::string stationPath)
{
	auto stationProxy = sdbus::createProxy(con, "net.connman.iwd", stationPath);
	auto method = stationProxy->createMethodCall("net.connman.iwd.Station", "RegisterSignalLevelAgent");
	std::vector<int16_t> levels = {-40, -50, -60};
	method << sdbus::ObjectPath(agentPath) << levels;
	stationProxy->callMethod(method);
}


int main(int argc, char *argv[])
{
	const char* serviceName = "org.sdbuscpp.iwdinfo";
	auto connection = sdbus::createSystemBusConnection(serviceName);
	const char* objectPath = "/org/sdbuscpp/iwdinfo";
	auto iwdinfo = sdbus::createObject(*connection, objectPath);
	std::string stationPath = getStationPath(*connection, "/net/connman/iwd/0");
	auto stationProxy = sdbus::createProxy(*connection, "net.connman.iwd", stationPath);
	auto changed = [&con=*connection, &stationPath](const sdbus::ObjectPath object, const uint8_t level)
	{
		auto stationProxy = sdbus::createProxy(con, "net.connman.iwd", stationPath);
		sdbus::ObjectPath networkPath = stationProxy->getProperty("ConnectedNetwork").onInterface("net.connman.iwd.Station");
		auto netProxy = sdbus::createProxy(con, "net.connman.iwd", networkPath);
		std::string name = netProxy->getProperty("Name").onInterface("net.connman.iwd.Network");
		std::cout << "{";
		std::cout << "\"objectPath\":\"" << object << "\",";
		std::cout << "\"name\":\"" << name << "\",";
		std::cout << "\"level\":\"" << (int) level << "\",";
		std::string icon = "󰤯";
		switch ((int) level){
			case 0:
				icon = "󰤨";
				break;
			case 1:
				icon = "󰤥";
				break;
			case 2:
				icon = "󰤢";
				break;
			case 3:
				icon = "󰤟";
				break;
		}
		std::cout << "\"icon\":\"" << icon << "\"";
		std::cout << "}" << std::endl;
		return;
	};
	auto release = [](const sdbus::ObjectPath object)
	{
		return;
	};
	const char* interfaceName = "net.connman.iwd.SignalLevelAgent";
	iwdinfo->registerMethod("Changed").onInterface(interfaceName).implementedAs(std::move(changed));
	iwdinfo->registerMethod("Release").onInterface(interfaceName).implementedAs(std::move(release));
	iwdinfo->finishRegistration();
	connection->enterEventLoopAsync();
	setup(*connection, serviceName, objectPath, stationPath);

	// Run the loop on the connection.
	connection->enterEventLoop();
	return 0;
}
