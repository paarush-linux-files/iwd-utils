#include <common.hpp>
#include <cstdint>
#include <iostream>
#include <map>
#include <vector>

int main(int argc, char* argv[])
{
	auto conn = sdbus::createConnection();
	auto stationPath = getStationPath(*conn, "/net/connman/iwd/0");
	auto stationProxy = sdbus::createProxy("net.connman.iwd", stationPath);
	std::vector<sdbus::Struct<sdbus::ObjectPath, int16_t>> networks;
	stationProxy->callMethod("GetOrderedNetworks").onInterface("net.connman.iwd.Station").storeResultsTo(networks);
	auto networkProxy = sdbus::createProxy("net.connman.iwd", networks.begin()->get<0>());
	std::cout << "{";
	std::cout << "\"objectPath\":\"" << stationPath << "\",";
	std::cout << "\"name\":\"" << (std::string) networkProxy->getProperty("Name").onInterface("net.connman.iwd.Network") << "\",";
	int level = networks.begin()->get<1>();
	if (level >= -4000){
		level = 0;
	} else if (level >= -5000){
		level = 1;
	} else if (level >= -6000){
		level = 2;
	} else {
		level = 3;
	}
	std::string icon = "󰤯";
	switch ((int) level){
		case 0:
			icon = "󰤨";
			break;
		case 1:
			icon = "󰤥";
			break;
		case 2:
			icon = "󰤢";
			break;
		case 3:
			icon = "󰤟";
			break;
	}
	std::cout << "\"level\":\"" << level << "\",";
	std::cout << "\"icon\":\"" << icon << "\"";
	std::cout << "}" << std::endl;
}
