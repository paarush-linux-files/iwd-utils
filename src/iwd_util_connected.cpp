#include <map>
#include <common.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>


int main(int argc, char *argv[])
{
	auto conn = sdbus::createConnection();
	const char* destinationName = "net.connman.iwd";
	auto objectPath = getStationPath(*conn, "/net/connman/iwd/0");
	auto concatenatorProxy = sdbus::createProxy(*conn, destinationName, objectPath);

	const char* interfaceName = "net.connman.iwd.Station";
	{
		std::string state = concatenatorProxy->getProperty("State").onInterface(interfaceName);
		std::cout << state << std::endl;
	}

	return 0;
}
